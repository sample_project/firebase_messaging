package ir.toseeit.fire_base_test_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ir.toseeit.fire_base_test_app.Push_Notification.MFirebaseMessagingService;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button add_btn, ret_btn;
    private TextView show_text;
    private EditText add_text;

    private DatabaseReference myRef;
    private String TAG = "read";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Define Wighet
        add_btn = (Button)findViewById(R.id.add_button);
        ret_btn = (Button)findViewById(R.id.Retrieve_button);
        show_text = (TextView)findViewById(R.id.textView);
        add_text = (EditText) findViewById(R.id.editText);

        add_btn.setOnClickListener(this);
        ret_btn.setOnClickListener(this);

        //Fire Base
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference("name");

        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                show_text.setText(value);
                Log.d(TAG, "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });


        //push notification

    }

    @Override
    public void onClick(View view) {
        if(view == add_btn){
            //Toast.makeText(getApplicationContext(),"add",Toast.LENGTH_LONG).show();

            // Write a message to the database


            myRef.setValue(add_text.getText().toString());

        }else if(view == ret_btn){


        }
    }
}
